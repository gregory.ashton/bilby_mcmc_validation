#!/usr/bin/env python
import glob

import bilby
import matplotlib.pyplot as plt

plt.style.use("../paper.mplstyle")

files = glob.glob("outdir_A/result/*json")
results = [bilby.core.result.read_in_result(f) for f in files]

keys = [
    "mass_1", "mass_2",
    "a_1", "a_2", "tilt_1", "tilt_2", "phi_12", "phi_jl",
    "azimuth", "zenith", "luminosity_distance",
    "cos_theta_jn", "psi", "phase", "geocent_time",
]

fig, pvals = bilby.core.result.make_pp_plot(
    results, save=False, legend_fontsize=4, keys=keys
)
ax = fig.gca()
ax.get_legend().set_frame_on(False)
fig.savefig("pp.png")
