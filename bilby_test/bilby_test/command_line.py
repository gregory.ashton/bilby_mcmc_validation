import sys


def command_line_setup():
    sampler_kwargs = {}
    label = None
    sampler = None

    # Add the kwargs from the command line
    for element in sys.argv[1:]:
        if element in ["-v", "-c"]:
            continue

        key, val = element.split("=")
        if key == "sampler":
            sampler = val
            continue
        if key == "label":
            label = val
            continue

        try:
            val = eval(val)
        except NameError:
            pass
        sampler_kwargs[key] = val

    if label is None:
        raise ValueError("label argument not given")

    if sampler is None:
        raise ValueError("sampler argument not given")

    outdir = f"outdir_{sampler}"

    # Set logging to store the outputs
    # bilby.core.utils.setup_logger(outdir=outdir, label=label)

    return sampler, sampler_kwargs, outdir, label
