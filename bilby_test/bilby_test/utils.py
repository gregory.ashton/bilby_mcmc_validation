import dill
from collections import namedtuple

import bilby
import bilby_pipe
import numpy as np
from scipy.spatial.distance import jensenshannon
from scipy.stats import gaussian_kde
from scipy.special import logsumexp


def calc_median_error(jsvalues, quantiles=(0.16, 0.84)):
    quants_to_compute = np.array([quantiles[0], 0.5, quantiles[1]])
    quants = np.percentile(jsvalues, quants_to_compute * 100)
    summary = namedtuple("summary", ["median", "lower", "upper"])
    summary.median = quants[1]
    summary.plus = quants[2] - summary.median
    summary.minus = summary.median - quants[0]
    return summary


def calculate_js(samplesA, samplesB, ntests=100, xsteps=100):
    js_array = np.zeros(ntests)
    for j in range(ntests):
        nsamples = min([len(samplesA), len(samplesB)])
        A = np.random.choice(samplesA, size=nsamples, replace=False)
        B = np.random.choice(samplesB, size=nsamples, replace=False)
        xmin = np.min([np.min(A), np.min(B)])
        xmax = np.max([np.max(A), np.max(B)])
        x = np.linspace(xmin, xmax, xsteps)
        A_pdf = gaussian_kde(A)(x)
        B_pdf = gaussian_kde(B)(x)

        js_array[j] = np.nan_to_num(np.power(jensenshannon(A_pdf, B_pdf), 2))

    return calc_median_error(js_array)


def read_result(result_file):
    return bilby.core.result.read_in_result(result_file)


def read_resume(resume_file):
    with open(resume_file, "rb") as file:
        resume = dill.load(file)
    return resume


def calculate_efficiency(resume, sampler):
    if sampler == "dynesty":
        logwts = np.array(resume.saved_logwt)
        logneff = logsumexp(logwts) * 2 - logsumexp(logwts * 2)
        neffsamples = np.exp(logneff)
        evaluations = resume.ncall
    elif sampler in ["bilby_mcmc", "bilbymcmc"]:
        neffsamples = (resume.nsamples * resume.convergence_inputs.thin_by_nact)
        evaluations = resume.position * resume.L1steps * resume.ntemps
    else:
        raise ValueError(f"Sampler {sampler} not supported")

    return evaluations, neffsamples, neffsamples / evaluations


def print_message(result_file, resume_file):
    result = read_result(result_file)
    resume = read_resume(resume_file)

    evaluations, neffsamples, efficiency = calculate_efficiency(resume, result.sampler)

    print(f"Analysed {result_file}")
    print(f"  bilby_version={result.meta_data['loaded_modules']['bilby']}")
    print(f"  bilby_pipe_version={result.meta_data['loaded_modules']['bilby_pipe']}")
    print(f"  Result lnZ={result.log_evidence:0.2f}+/-{result.log_evidence_err:0.2f}")
    print(f"  Sampling_time={result.sampling_time:0.2f}s")
    print(f"  Efficiency={efficiency * 100:0.4f}%")
    print(f"  Evaluations: {evaluations:1.3g}")
    print(f"  Neff-samples: {neffsamples:1.3g}")
    print(f"  Nsamples: {len(result.posterior):1.3g}")
    if result.sampler == "bilby_mcmc":
        tau = str(int(resume.tau))
        print(f"  tau: {resume.tau}")
        print(f"  steps: {resume.position}")
    else:
        tau = "---"

    print(f"  latex= {tau} & {efficiency * 100:0.4f}\% & {evaluations/1e6:1.3g} & {int(neffsamples)}")

    return result, resume
