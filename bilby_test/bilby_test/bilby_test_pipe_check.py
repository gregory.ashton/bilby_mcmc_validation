import argparse
import os

import dill
import glob

import bilby
import numpy as np
from scipy.special import logsumexp


def get_args():
    parser = argparse.ArgumentParser("bilby_test_pipe_check")
    parser.add_argument("outdir", help="Path to the bilby_piep outdir")
    return parser.parse_args()


def read_files(args):
    if os.path.exists(f"{args.outdir}/result") is False:
        raise ValueError("Directory not found")

    resume_file_paths = glob.glob(f"{args.outdir}/result/*_resume.pickle")
    if len(resume_file_paths) == 0:
        raise ValueError("Resume files not found")

    resume_files = []
    for resume_file in resume_file_paths:
        with open(resume_file, "rb") as file:
            resume_files.append(dill.load(file))

    result_file_paths = glob.glob(f"{args.outdir}/result/*_result.json")
    if len(result_file_paths) == 0:
        raise ValueError("Result files not found")

    result_files = []
    final_result = None
    for result_file in result_file_paths:
        if "PRELIM" in result_file:
            pass
        elif "merge" in result_file:
            final_result = bilby.core.result.read_in_result(result_file)
        else:
            result_files.append(bilby.core.result.read_in_result(result_file))

    if final_result is None:
        if len(result_files) == 1:
            final_result = result_files[0]
        else:
            raise ValueError("Results have not been merged")

    sampler = final_result.sampler

    return final_result, result_files, resume_files, sampler


def check_bilby_mcmc(final_result, result_files, resume_files):
    evaluations = []
    nsamples = []
    neffsamples = []
    taus = []
    for resume in resume_files:
        taus.append(resume.tau)
        evals = resume.position * resume.L1steps * resume.ntemps
        evaluations.append(evals)
        nsamples.append(resume.nsamples)
        neffsamples.append(resume.nsamples * resume.convergence_inputs.thin_by_nact)

    sampling_times = []
    for result in result_files:
        sampling_times.append(result.sampling_time)

    efficiency = np.sum(neffsamples) / np.sum(evaluations)

    print(f"Using {len(result_files)} parallel samplers:")
    print(f"  Result lnBF={final_result.log_bayes_factor:0.2f}+/-{final_result.log_evidence_err:0.2f}")
    print(f"  Result lnZ={final_result.log_evidence:0.2f}+/-{final_result.log_evidence_err:0.2f}")
    print(f"  L1steps={resume.convergence_inputs.L1steps}")
    print(f"  tau={np.mean(taus):0.2f}\\pm{np.std(taus):0.2f}")
    print(f"  tau-range={np.min(taus):0.2f}-{np.max(taus):0.2f}")
    print(f"  evaluations={np.sum(evaluations):0.2g}")
    if len(sampling_times) > 0:
        print(f"  maximimum sampling time={np.max(sampling_times):0.1f}s")
    print(f"  total effective samples={np.sum(neffsamples):0.0f}")
    print(f"  efficiency={100 * efficiency:0.5f}%")


def check_dynesty(final_result, result_files, resume_files):
    n = len(resume_files)
    evaluations = []
    neffsamples = []
    for resume in resume_files:
        evaluations.append(resume.ncall)
        logwts = np.array(resume.saved_logwt)
        logneff = logsumexp(logwts) * 2 - logsumexp(logwts * 2)
        neffsamples.append(np.exp(logneff))

    sampling_time = []
    for result in result_files:
        sampling_time.append(result.sampling_time)

    efficiency = np.sum(neffsamples) / np.sum(evaluations)

    print(f"Using {n} parallel samplers:")
    print(f"  Result lnBF={final_result.log_bayes_factor:0.2f}+/-{final_result.log_evidence_err:0.2f}")
    print(f"  Result lnZ={final_result.log_evidence:0.2f}+/-{final_result.log_evidence_err:0.2f}")
    print(f"  evaluations={np.sum(evaluations):0.2g}")
    print(f"  maximimum sampling time={np.max(sampling_time):0.1f}s")
    print(f"  total samples={len(final_result.posterior):0.0f}")
    print(f"  total effective samples={np.sum(neffsamples):0.0f}")
    print(f"  efficiency={100 * efficiency:0.5f}%")


methods = dict(
    bilby_mcmc=check_bilby_mcmc,
    bilbymcmc=check_bilby_mcmc,
    dynesty=check_dynesty
)


def main():
    args = get_args()
    final_result, result_files, resume_files, sampler = read_files(args)
    if sampler in methods:
        methods[sampler](final_result, result_files, resume_files)
    else:
        raise ValueError(f"Can't process results for sampler {sampler}")
