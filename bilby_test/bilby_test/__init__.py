import bilby
import bilby_pipe

from .utils import calculate_js
from .standard_tests import get_likelihood_prior_and_sample, available_tests
from .command_line import command_line_setup

__version__ = "0.0.1"


def print_msg(file=None):
    print("Software versions:", file=file)
    print(f"  bilby={bilby.__version__}", file=file)
    print(f"  bilby_pipe={bilby_pipe.__version__}", file=file)
    print(f"  bilby_test={__version__}", file=file)
