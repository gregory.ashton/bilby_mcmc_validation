#!/usr/bin/env python

from setuptools import setup


setup(name='bilby_test',
      description='A library of tools to test/validate bilby',
      author='Greg Ashton',
      author_email='gregory.ashton@ligo.org',
      license="MIT",
      packages=['bilby_test'],
      package_dir={'bilby_test': 'bilby_test'},
      python_requires='>=3.5',
      entry_points={
          "console_scripts": ["bilby_test_pipe_check=bilby_test.bilby_test_pipe_check:main"]
      },
      classifiers=[
          "Programming Language :: Python :: 3.6",
          "Programming Language :: Python :: 3.7",
          "License :: OSI Approved :: MIT License",
          "Operating System :: OS Independent"])
