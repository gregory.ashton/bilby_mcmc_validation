# Bilby MCMC validation

A repository which hosts the source files to reproduce the validation of the
Bilby-MCMC sampler.

## Setup

Many of the tests rely on a `bilby_test` package contained in this repo. To install this software:
```console
>>> cd bilby_test
>>> pip install .
```

## Running tests
For each directory, there is a `Makefile` to run the tests. Please read these
for detailed instructions on what needs to be run. For many tests, the analysis
time can take several hours. If working on a HTCondor cluster, the Makefile
will automatically submit your jobs. But, you may need to wait for them to
finish before producing output-style plots.
