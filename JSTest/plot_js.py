import matplotlib.pyplot as plt
import numpy as np

plt.style.use("../paper.mplstyle")

N, js = np.loadtxt("max_js_values.txt").T

fig, ax = plt.subplots()
ax.scatter(1e-3 * N, 1e-3 / js, marker="o", s=3, alpha=0.5, c='k')
ax.set_ylabel("1 / Max-JS [$10^{3}$]")
ax.set_xlabel("Number of samples [$10^{3}$]")

x = np.linspace(0, 50)
ylower = 0.1 * x
ax.fill_between(x, ylower, 10.2, alpha=0.2, color='C1', zorder=-100)
ax.plot(x, ylower, color='C1', ls='--')

bilby2_threshold = 1e-3 / 0.002
ax.axhline(bilby2_threshold, alpha=0.3, color='C0', zorder=-100, ls='--')
ax.fill_between(x, bilby2_threshold, ylower, alpha=0.3, color='C0', zorder=-100)


ax.set_xlim(0, 50)
ax.set_ylim(0, 10)

fig.tight_layout()
fig.savefig("js_vs_N.png")
