import os
import sys

import numpy as np

sys.path.append(os.path.abspath("../"))
from btutils import calculate_js

cov = np.genfromtxt("../15D_Gaussian_unimodal/covariance.txt", delimiter=",")
ndim = len(cov)
mean = np.zeros(ndim)

for _ in range(1000):
    N = int(np.random.randint(10, 50000))
    print(N)
    datasetA = np.random.multivariate_normal(mean, cov, size=N)
    datasetB = np.random.multivariate_normal(mean, cov, size=N)
    js = []
    for i in range(ndim):
        js.append(calculate_js(datasetA[:, i], datasetB[:, i], 1).median)
    with open("max_js_values.txt", "a+") as f:
        print(f"{N} {np.max(js):1.3g}", file=f, flush=True)
