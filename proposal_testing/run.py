#!/usr/bin/env python

import copy
import argparse

import numpy as np
from tqdm import tqdm

from bilby.core.sampler.bilby_mcmc.proposals import get_proposal_cycle
from bilby.core.sampler.bilby_mcmc.chain import Chain, Sample
from bilby.core.sampler.bilby_mcmc.utils import LOGLKEY, LOGPKEY
from bilby_test import get_likelihood_prior_and_sample, print_msg, available_tests

np.random.seed(123)

parser = argparse.ArgumentParser()
parser.add_argument(
    "test", help="The name of the test to run", choices=available_tests.keys()
)
parser.add_argument(
    "proposal_string", help="The name of the proposal string to use"
)

parser.add_argument(
    '-n', "--per-proposal", default=10000, type=int, help="Number of steps per proposal"
)
args = parser.parse_args()

test = args.test
likelihood, priors, initial_sample = get_likelihood_prior_and_sample(test)


def calculate_likelihood_and_prior(sample):
    sample[LOGPKEY] = priors.ln_prob(sample.parameter_only_dict)
    if np.isneginf(sample[LOGPKEY]):
        sample[LOGLKEY] = -np.inf
    else:
        likelihood.parameters.update(sample.sample_dict)
        sample[LOGLKEY] = likelihood.log_likelihood()
    return sample


proposal_cycle = get_proposal_cycle(args.proposal_string, priors)

# Create initial sample
initial_sample = Sample(initial_sample)
initial_sample = calculate_likelihood_and_prior(initial_sample)
chain = Chain(initial_sample=initial_sample)

N = args.per_proposal * len(proposal_cycle.proposal_list)

# Log outputs
outfile = f"summary_{test}.txt"
with open(outfile, 'w+') as file:
    print_msg(file=file)
    print(f"Running for test={test} with {args.per_proposal} steps per proposal", file=file)
    print("Priors:", file=file)
    for key in priors:
        print(f"  {key}: {priors[key]}", file=file)

for _ in tqdm(range(N)):
    curr = chain.current_sample
    proposal = proposal_cycle.get_proposal()
    prop, log_factor = proposal(chain)
    prop = calculate_likelihood_and_prior(prop)

    log_alpha = log_factor + prop[LOGLKEY] + prop[LOGPKEY] - curr[LOGLKEY] - curr[LOGPKEY]

    if np.random.uniform(0, 1) <= np.exp(log_alpha):
        proposal.accepted += 1
        curr = copy.deepcopy(prop)
        chain.current_sample = curr
        chain.append(curr)
    else:
        proposal.rejected += 1
        chain.append(curr)

chain.plot(outdir=".", label=f"{test}_{args.proposal_string}")
with open(outfile, 'a+') as file:
    print(proposal_cycle, file=file)
