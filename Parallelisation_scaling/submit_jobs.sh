
for L1steps in 10 100 ; do \
    for cpus in 1 2 4 6 8 10 12 14 16 ; do \
        for run in A B C ; do \
            ../runjobs.sh run.py "fixed_tau=1 label=mcmc-${L1steps}-${cpus}-${run} sampler=bilby_mcmc nsamples=50 L1steps=${L1steps} L2steps=5 ntemps=16 proposal_cycle='default_noKDnoNFnoGM' wait=10 printdt=30 npool=${cpus}"
        done
    done
done
