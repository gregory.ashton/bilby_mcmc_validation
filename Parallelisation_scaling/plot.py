import os
import glob

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import bilby
import matplotlib.ticker as ticker

plt.style.use("../paper.mplstyle")


rows = []
for directory in glob.glob("outdir*"):
    result_files = glob.glob(f"{directory}/*json")
    for rf in result_files:
        res = bilby.core.result.read_in_result(rf)
        rf_list = os.path.basename(rf).split("_")[0].split("-")

        rows.append(dict(
            L1steps=int(rf_list[1]),
            npool=int(rf_list[2]),
            run=rf_list[3],
            nsamples=len(res.posterior),
            sampling_time=res.sampling_time,
            wait=10,
        ))

df = pd.DataFrame(rows)

fig, ax = plt.subplots()
nsamples_target = 50
colors = ["C0", "C1", "C2"]
L1steps = [10, 100]
for wait in df.wait.unique():
    for L1steps, col in zip(L1steps, colors):
        dfX = df[(df.wait == wait) & (df.L1steps == L1steps)].sort_values("npool")
        dfXmean = dfX.groupby("npool").mean()
        dfXstd = dfX.groupby("npool").std()
        npool = dfXmean.index.values
        sampling_time = dfXmean.sampling_time.values
        sampling_time_std = dfXstd.sampling_time.values
        if len(dfX) > 0:
            base_time = sampling_time[0]
            speed_up = base_time / sampling_time
            err = 3 * speed_up * (sampling_time_std / sampling_time)
            label = f"$t={wait}$ ms, $L_1={L1steps}$"
            ax.plot(npool, speed_up, '-', label=label, ms=2, color=col)
            #ax.errorbar(npool, speed_up, err, fmt="-", label=label)
            ax.fill_between(npool, speed_up - err, speed_up + err,
                            alpha=0.2, color=col, linewidth=0)


ncpus = np.arange(1, 17, 1)
plt.plot(ncpus, ncpus, "-k")
for m in [0.75, 0.5, 0.25]:
    plt.fill_between(ncpus, m * ncpus, ncpus, color='k', alpha=0.1, zorder=-10,
                     linewidth=0)

axR = ax.twinx()
axR.set_ylim(1, 16)
axR.set_yticks([4, 8, 12, 16])
axR.minorticks_off()
axR.set_yticklabels(["1/4", "1/2", "3/4", "1"], color='gray')

ax.legend(fontsize=8, frameon=False)
ax.set_xlabel(r"$n_{\rm cores}$")
ax.set_ylabel("Speedup")
ax.xaxis.set_major_locator(ticker.MultipleLocator(4))
ax.minorticks_off()
ax.yaxis.set_major_locator(ticker.MultipleLocator(4))
ax.set_xlim(1, 16)
ax.set_ylim(1, 16)
fig.tight_layout()
fig.savefig("speed_up")
