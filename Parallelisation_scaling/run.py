#!/usr/bin/env python

import time

import bilby
import numpy as np

from bilby_test import command_line_setup

np.random.seed(123)

sampler, sampler_kwargs, outdir, label = command_line_setup()


class TimedLikelihood(bilby.Likelihood):
    def __init__(self, wait=0):
        super().__init__(parameters={'a': None})

    def log_likelihood(self):
        a = self.parameters['a']
        time.sleep(wait * 1e-3)
        return - a ** 2


wait = sampler_kwargs.get("wait", 0)
outdir += f"_wait{wait}"

likelihood = TimedLikelihood(wait)
priors = dict(a=bilby.core.prior.Uniform(-5, 5, 'mu'))
result = bilby.run_sampler(
    likelihood=likelihood, priors=priors, outdir=outdir, label=label,
    sampler=sampler, **sampler_kwargs
)
