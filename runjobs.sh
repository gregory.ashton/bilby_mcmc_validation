#!/bin/bash
# A script to either submit jobs locally or using the HTCondor queue

# Readin the environment variable
USE_SCHEDULER=${USE_SCHEDULER:-true}

# Check if condor_submit exists
if ! command -v condor_submit &> /dev/null
then
    echo "WARNING: condor_submit could not be found: defaulting to local run"
    USE_SCHEDULER=false
fi

exe=$1
args=$2
rundir=${PWD##*/}

request_cpus=1
for pair in $args
do
    if [[ $pair == *"label="* ]]; then
        label=$(python -c 'import sys; print(sys.argv[1].split("=")[1])' $pair)
    fi

    if [[ $pair == *"npool="* ]]; then
        request_cpus=$(python -c 'import sys; print(sys.argv[1].split("=")[1])' $pair)
    fi
done

batch_name="$rundir-$label"

# If use the scheduler, submit jobs under HTCondor, else locally
if [ "$USE_SCHEDULER" = true ] ; then
    mkdir logs/ -p
    condor_submit ../htcondor.submit executable=$exe arguments="$args" batch_name=$batch_name label=$label request_cpus=$request_cpus
else
    ./$exe $args
fi
