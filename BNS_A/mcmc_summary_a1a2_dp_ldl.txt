Using 13 parallel samplers:
  Result lnBF=132.60+/-0.28
  Result lnZ=-257461.64+/-0.28
  L1steps=100
  tau=578.86\pm97.79
  tau-range=439.40-760.20
  evaluations=2.5e+09
  maximimum sampling time=482178.5s
  total effective samples=5209
  efficiency=0.00021%
