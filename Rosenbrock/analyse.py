import sys

import numpy as np

from bilby_test import get_likelihood_prior_and_sample, calculate_js
from bilby_test.utils import read_result, print_message


def draw_rosenbrock(limit=5, n_samples=100000):
    """
    Draw from a 2D Rosenbrock distribution

    This is done by reparameterizing the problem as

    z = 10 (y - x^2)
    w = x - 1

    so that

    lnL = - (w^2 + z^2)
    """
    z = np.random.normal(0, 2 ** -0.5, n_samples * 2)
    w = np.random.normal(0, 2 ** -0.5, n_samples * 2)

    x = w + 1
    y = z / 10 + x ** 2

    keep = (abs(x) < 5) & (abs(y) < 5)

    x = x[keep][:n_samples]
    y = y[keep][:n_samples]
    return x, y


likelihood, priors, _ = get_likelihood_prior_and_sample("rosenbrock")

log_evidence = -5.804


# Pass directories with results on the command line
outdir = sys.argv[1]
label = sys.argv[2]
result_file = f"{outdir}/{label}_result.json"
resume_file = f"{outdir}/{label}_resume.pickle"


# draw samples from the posterior
N = 5000
dataset = np.array(draw_rosenbrock(n_samples=N)).T

# Read in the result file
result = read_result(result_file)

pos = result.posterior.sample(N)

js_medians = []
for ii, key in enumerate(result.search_parameter_keys):
    samplesA = result.posterior[key]
    samplesB = dataset[:, ii]
    js_medians.append(calculate_js(samplesA, samplesB).median)

print("Analysing Rosenbrock")
result, resume = print_message(result_file, resume_file)

print(f"  Analytic lnZ={log_evidence:0.2f}")
print(f"  delta lnZ = {result.log_evidence - log_evidence} \\pm {result.log_evidence_err}")
print(f"  Maximum JS: {max(js_medians):1.3g}")
