.PHONY : results tests

# List of results
dynestyA = outdir_dynesty/dynestyA_result.json
dynestyB = outdir_dynesty/dynestyB_result.json
mcmcA = outdir_bilby_mcmc/mcmcA_result.json
mcmcB = outdir_bilby_mcmc/mcmcB_result.json
mcmcC = outdir_bilby_mcmc/mcmcC_result.json
mcmcD = outdir_bilby_mcmc/mcmcD_result.json
mcmcE = outdir_bilby_mcmc/mcmcE_result.json
mcmcF = outdir_bilby_mcmc/mcmcF_result.json
mcmcG = outdir_bilby_mcmc/mcmcG_result.json
files = $(dynestyA) $(dynestyB) $(mcmcA) $(mcmcB) $(mcmcC) $(mcmcD) $(mcmcE) $(mcmcF) $(mcmcG)

tests: summary.txt js.txt rosenbrock_all.png rosenbrock_mcmc.png rosenbrock_dynesty.png rosenbrock_simple.png

results: $(files) evidence_validation.txt

# Create outputs
summary.txt:
	python analyse.py outdir_dynesty dynestyA > summary.txt
	python analyse.py outdir_dynesty dynestyB >> summary.txt
	python analyse.py outdir_bilby_mcmc mcmcA >> summary.txt
	python analyse.py outdir_bilby_mcmc mcmcB >> summary.txt
	python analyse.py outdir_bilby_mcmc mcmcC >> summary.txt
	python analyse.py outdir_bilby_mcmc mcmcD >> summary.txt
	python analyse.py outdir_bilby_mcmc mcmcE >> summary.txt
	python analyse.py outdir_bilby_mcmc mcmcF >> summary.txt
	python analyse.py outdir_bilby_mcmc mcmcG >> summary.txt

rosenbrock_simple.png:
	python plot.py dynesty=$(dynestyA) mcmc=$(mcmcA) out=rosenbrock_simple.png

rosenbrock_all.png:
	python plot.py dynestyA=$(dynestyA) dynestyB=$(dynestyB) mcmc-A=$(mcmcA) mcmc-B=$(mcmcB) mcmc-C=$(mcmcC) mcmc-D=$(mcmcD) mcmc-E=$(mcmcE)  mcmc-F=$(mcmcF) mcmc-G=$(mcmcG) out=rosenbrock_all.png

rosenbrock_dynesty.png:
	python plot.py dynesty-A=$(dynestyA) dynesty-B=$(dynestyB) out=rosenbrock_dynesty.png

rosenbrock_mcmc.png:
	python plot.py mcmc-A=$(mcmcA) mcmc-B=$(mcmcB) mcmc-C=$(mcmcC) mcmc-D=$(mcmcD) mcmc-E=$(mcmcE) mcmc-F=$(mcmcF) mcmc-G=$(mcmcG) out=rosenbrock_mcmc.png

js.txt:
	python js.py > js.txt

# Create results files
dynestyA: $(dynestyA)
dynestyB: $(dynestyB)
mcmcA: $(mcmcA)
mcmcB: $(mcmcB)
mcmcC: $(mcmcC)
mcmcD: $(mcmcD)
mcmcE: $(mcmcE)
mcmcF: $(mcmcF)
mcmcG: $(mcmcG)

$(dynestyA):
	../runjobs.sh run.py "sampler=dynesty nlive=2000 nact=50 label=dynestyA"

$(dynestyB):
	../runjobs.sh run.py "sampler=dynesty nlive=2000 nact=10 label=dynestyB"

$(mcmcA):
	../runjobs.sh run.py "sampler=bilby_mcmc nsamples=20000 L1steps=1 proposal_cycle="default" label=mcmcA"

$(mcmcB):
	../runjobs.sh run.py "sampler=bilby_mcmc nsamples=20000 L1steps=1 proposal_cycle="default_noKDnoGM" label=mcmcB"

$(mcmcC):
	../runjobs.sh run.py "sampler=bilby_mcmc nsamples=20000 L1steps=1 proposal_cycle="default_noNFnoGM" label=mcmcC"

$(mcmcD):
	../runjobs.sh run.py "sampler=bilby_mcmc nsamples=20000 L1steps=1 proposal_cycle="default_noNFnoKD" label=mcmcD"

$(mcmcE):
	../runjobs.sh run.py "sampler=bilby_mcmc nsamples=20000 L1steps=1 proposal_cycle="default_noNFnoKDnoGM" label=mcmcE"

$(mcmcF):
	../runjobs.sh run.py "sampler=bilby_mcmc nsamples=20000 L1steps=1 proposal_cycle="default" ntemps=8 Tmax=10000000 label=mcmcF"

$(mcmcG):
	../runjobs.sh run.py "sampler=bilby_mcmc nsamples=20000 L1steps=1 proposal_cycle="default" ntemps=16 Tmax=10000000 label=mcmcG"

evidence_validation.txt:
	../runjobs.sh run_evidence_validation.py "sampler=bilby_mcmc label=evidence"

clean:
	rm *png -f
	rm summary.txt -f

deep_clean:
	rm outdir* -rf
	rm logs/*
	rm *png -f
