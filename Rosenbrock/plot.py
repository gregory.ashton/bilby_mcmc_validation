import sys
from copy import deepcopy

import numpy as np
import matplotlib.pyplot as plt
import bilby

plt.style.use("../paper.mplstyle")


def draw_rosenbrock(limit=5, n_samples=100000):
    """
    Draw from a 2D Rosenbrock distribution

    This is done by reparameterizing the problem as

    z = 10 (y - x^2)
    w = x - 1

    so that

    lnL = - (w^2 + z^2)
    """
    z = np.random.normal(0, 2 ** -0.5, n_samples * 2)
    w = np.random.normal(0, 2 ** -0.5, n_samples * 2)

    x = w + 1
    y = z / 10 + x ** 2

    keep = (abs(x) < 5) & (abs(y) < 5)

    x = x[keep][:n_samples]
    y = y[keep][:n_samples]
    return x, y


filename = "rosenbrock.png"

results = []
labels = []
for item in sys.argv[1:]:
    label, file = item.split("=")
    if label == "out":
        filename = file
    else:
        results.append(bilby.core.result.read_in_result(file))
        labels.append(label)

exact_result = deepcopy(results[0])
exact_result.posterior["x"], exact_result.posterior["y"] = draw_rosenbrock(
    n_samples=len(exact_result.posterior)
)
results.append(exact_result)
labels.append("direct")

bilby.result.plot_multiple(
    results, filename=filename, labels=labels, dpi=600, bins=30,
    plot_datapoints=False, fill_contours=False,
)
