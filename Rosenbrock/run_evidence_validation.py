#!/usr/bin/env python

import bilby
import numpy as np

from bilby_test import command_line_setup, get_likelihood_prior_and_sample, print_msg

print_msg()
np.random.seed(123)

sampler, sampler_kwargs, _, _ = command_line_setup()

likelihood, priors, _ = get_likelihood_prior_and_sample("rosenbrock")

# Set some defaults
sampler_kwargs.update(dict(
    nsamples=500,
    proposal_cycle='default_noNFnoGMnoKD',
    ntemps=8,
    Tmax=10000000
))

outdir = f"outdir_{sampler}_evidence_test"

log_evidence_list = []
log_evidence_uncertainty_list = []
for i in range(3):
    label = f"run{i}"
    result = bilby.run_sampler(
        likelihood=likelihood,
        priors=priors,
        sampler=sampler,
        outdir=outdir,
        label=label,
        check_point_plot=False,
        resume=True,
        printdt=10,
        **sampler_kwargs
    )
    log_evidence_list.append(result.log_evidence)
    log_evidence_uncertainty_list.append(result.log_evidence_err)

with open("evidence_validation.txt", "w+") as f:
    print(f"Empirical Mean evidence = {np.mean(log_evidence_list)}", file=f)
    print(f"Empirical Std Dev. evidence = {np.std(log_evidence_list)}", file=f)
    print(f"Std Dev. Mean = {np.mean(log_evidence_uncertainty_list)}", file=f)
    print(f"Std Dev. Std Dev = {np.std(log_evidence_uncertainty_list)}", file=f)
