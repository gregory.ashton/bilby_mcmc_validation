import glob
import os

import bilby

from bilby_test import calculate_js

files = glob.glob("outdir_bilby_mcmc/*json")
files += glob.glob("outdir_dynesty/*json")
results = []
names = []
for file in files:
    results.append(bilby.core.result.read_in_result(file))
    names.append(os.path.basename(file).replace("_result.json", ""))
    print(results[-1].label, len(results[-1].posterior))

N = len(results)
for ii in range(N):
    for jj in range(ii + 1, N):
        rA = results[ii]
        rB = results[jj]
        for key in rA.search_parameter_keys:
            samplesA = rA.posterior.sample(5000)[key].values
            samplesB = rB.posterior.sample(5000)[key].values
            js = calculate_js(samplesA, samplesB, 100)
            print(f"JS({key}: {names[ii]}-{names[jj]}: {js.median} (+{js.plus} -{js.minus})", flush=True)
