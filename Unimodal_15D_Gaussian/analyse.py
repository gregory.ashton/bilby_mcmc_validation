import sys

import matplotlib.pyplot as plt
import numpy as np

from bilby_test.standard_tests import cov
from bilby_test import get_likelihood_prior_and_sample, calculate_js
from bilby_test.utils import print_message

plt.style.use("../paper.mplstyle")

likelihood, priors, _ = get_likelihood_prior_and_sample("unimodal_15D_gaussian")
ndim = 15
mean = np.zeros(ndim)

# The prior is constant and flat, and the likelihood is normalised such
# that the area under it is one. The analytical evidence is then given
# as 1/(prior volume).

log_prior_vol = np.sum(np.log(
    [prior.maximum - prior.minimum for key, prior in priors.items()]
))
log_evidence = -log_prior_vol


def add_violin(ax, dataset, offset=0, xspace=2, label=None, ndim=5):
    dataset = dataset[:, :ndim]
    positions = np.arange(0, xspace * ndim, xspace) + offset
    ax.violinplot(
        dataset, positions=positions, showmedians=True, vert=True,
        showextrema=False, quantiles=[[0.05, 0.95]] * ndim,
    )
    return ax, positions


# Pass directories with results on the command line
outdir = sys.argv[1]
label = sys.argv[2]
result_file = f"{outdir}/{label}_result.json"
resume_file = f"{outdir}/{label}_resume.pickle"

# Set up plot parameters
xspace = 2
dx = 0.5
left = dx / 2


fig, ax = plt.subplots()

# Add expected violin plot
N = 5000
dataset = np.random.multivariate_normal(mean, cov, size=N)
ax, _ = add_violin(ax, dataset, label="Direct", offset=left, xspace=xspace)

# Read in the result file
result, resume = print_message(result_file, resume_file)

pos = result.posterior.sample(N)
dataset = np.array(pos[priors.keys()])
ax, pos = add_violin(
    ax, dataset, offset=left + dx, label=result.sampler)

ax.set_xticks(pos - dx / 2)
ax.set_xticklabels([f"$x_{i}$" for i, _ in enumerate(pos)])
ax.set_ylabel("$p(x_{i})$")
ax.set_ylim(-0.75, 0.75)
fig.tight_layout()
fig.savefig(f"{outdir}/{label}_violin.png")

js_medians = []
for ii, key in enumerate(result.search_parameter_keys):
    samplesA = result.posterior[key]
    samplesB = dataset[:, ii]
    js_medians.append(calculate_js(samplesA, samplesB).median)


dlogz = result.log_evidence - log_evidence
print(f"  Analytic lnZ={log_evidence:0.2f}")
print(f"  delta lnZ = {dlogz} \\pm {result.log_evidence_err}")
print(f"  Maximum JS: {1e3 * max(js_medians):1.3g} [mb]")
