import matplotlib.pyplot as plt
import numpy as np
from bilby.core.result import read_in_result

from bilby_test.standard_tests import cov
from bilby_test import get_likelihood_prior_and_sample

plt.style.use("../paper.mplstyle")

likelihood, priors, _ = get_likelihood_prior_and_sample("unimodal_15D_gaussian")
ndim = 15
mean = np.zeros(ndim)


def add_violin(ax, dataset, offset=0, xspace=2, label=None, ndim=5, color='k'):
    dataset = dataset[:, :ndim]
    positions = np.arange(0, xspace * ndim, xspace) + offset
    vps = ax.violinplot(
        dataset, positions=positions, showmedians=True, vert=True,
        showextrema=False, quantiles=[[0.05, 0.95]] * ndim,
    )
    for vp in vps['bodies']:
        vp.set_facecolor(color)
        vp.set_edgecolor(color)
    for partname in ('cquantiles', 'cmedians'):
        vp = vps[partname]
        vp.set_edgecolor(color)
        vp.set_linewidth(0.5)
    return ax, positions


# Pass directories with results on the command line
dynesty = read_in_result("outdir_dynesty/dynestyA_result.json")
mcmc = read_in_result("outdir_bilby_mcmc/mcmcA_result.json")

# Set up plot parameters
xspace = 2
dx = 0.5
left = dx / 2

fig, ax = plt.subplots()

# Add expected violin plot
N = 5000
dataset = np.random.multivariate_normal(mean, cov, size=N)
ax, _ = add_violin(
    ax, dataset, label="Direct", offset=left, xspace=xspace, color='gray',
)

dynesty_dataset = np.array(dynesty.posterior.sample(N)[priors.keys()])
ax, pos = add_violin(ax, dynesty_dataset, offset=left + dx, label='dynesty', color='C0')

mcmc_dataset = np.array(mcmc.posterior.sample(N)[priors.keys()])
ax, pos = add_violin(ax, mcmc_dataset, offset=left + 2 * dx, label='mcmc', color='C1')

ax.set_xticks(pos - dx)
plt.tick_params(axis="x", which="minor", bottom=False)
ax.set_xticklabels([f"$x_{i}$" for i, _ in enumerate(pos)])
ax.set_ylabel("$p(x_{i})$")
ax.set_ylim(-0.75, 0.75)
fig.tight_layout()
fig.savefig("comparision_violin.png")
