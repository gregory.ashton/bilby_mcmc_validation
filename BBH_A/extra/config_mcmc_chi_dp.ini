################################################################################
## Calibration arguments
################################################################################

calibration-model=None
spline-calibration-envelope-dict=None
spline-calibration-nodes=5
spline-calibration-amplitude-uncertainty-dict=None
spline-calibration-phase-uncertainty-dict=None

################################################################################
## Data generation arguments
################################################################################

ignore-gwpy-data-quality-check=True
gps-tuple=None
gps-file=None
timeslide-file=None
timeslide-dict=None
trigger-time=None
n-simulation=0
data-dict=None
data-format=None
channel-dict=None
gaussian-noise=True
zero-noise=False

################################################################################
## Detector arguments
################################################################################

coherence-test=False
detectors=['H1', 'L1']
duration=4
generation-seed=1010
psd-dict=None
psd-fractional-overlap=0.5
post-trigger-duration=2.0
sampling-frequency=4096
psd-length=32
psd-maximum-duration=1024
psd-method=median
psd-start-time=None
maximum-frequency=1024
minimum-frequency=20
tukey-roll-off=0.4
resampling-method=lal

################################################################################
## Injection arguments
################################################################################

injection=True
injection-dict={'chirp_mass': 17.051544979894693, 'mass_ratio': 0.6183945489993522, 'chi_1': 0.29526500202350264, 'chi_2': 0.13262056301313416, 'chi_1_in_plane': 0.0264673717225983, 'chi_2_in_plane': 0.3701305583885513, 'phi_12': 1.0962562029664955, 'phi_jl': 0.518241237045709, 'luminosity_distance': 497.2983560174788, 'dec': 0.2205292600865073, 'ra': 3.952677097361719, 'theta_jn': 1.8795187965094322, 'psi': 2.6973435044499543, 'phase': 3.686990398567503, 'geocent_time': 0.040833669551002205}
injection-file=None
injection-numbers=None
injection-waveform-approximant=None

################################################################################
## Job submission arguments
################################################################################

accounting=ligo.dev.o3.cbc.pe.lalinference
label=bbh_A
outdir=outdir_mcmc_chi_dp_A
local=False
local-generation=True
local-plot=False
overwrite-outdir=False
periodic-restart-time=28800
request-memory=8.0
request-memory-generation=None
singularity-image=None
scheduler=condor
scheduler-args=None
scheduler-module=None
scheduler-env=None
scheduler-analysis-time=7-00:00:00
submit=False
condor-job-priority=0
transfer-files=False
log-directory=None
online-pe=False
osg=False
analysis-executable=None

################################################################################
## Likelihood arguments
################################################################################

distance-marginalization=True
distance-marginalization-lookup-table=TD.npz
phase-marginalization=False
time-marginalization=True
jitter-time=True
reference-frame=H1L1
time-reference=geocent
likelihood-type=GravitationalWaveTransient
roq-folder=None
roq-weights=None
roq-scale-factor=1
extra-likelihood-kwargs=None

################################################################################
## Output arguments
################################################################################

create-plots=False
plot-calibration=False
plot-corner=False
plot-marginal=False
plot-skymap=False
plot-waveform=False
plot-format=png
create-summary=True
email=None
notification=Never
existing-dir=None
webdir=None
summarypages-arguments=None

################################################################################
## Prior arguments
################################################################################

default-prior=BBHPriorDict
deltaT=0.2
prior-dict={
chirp_mass=bilby.gw.prior.UniformInComponentsChirpMass(name='chirp_mass', minimum=16, maximum=18),
mass_ratio=bilby.gw.prior.UniformInComponentsMassRatio(name='mass_ratio', minimum=0.125, maximum=1),
mass_1=Constraint(name='mass_1', minimum=1.001398, maximum=1000),
mass_2=Constraint(name='mass_2', minimum=1.001398, maximum=1000),
chi_1=bilby.gw.prior.AlignedSpin(name="chi_1", latex_label="$\\chi_1$"),
chi_2=bilby.gw.prior.AlignedSpin(name="chi_2", latex_label="$\\chi_2$"),
chi_1_in_plane=bilby.gw.prior.ConditionalChiInPlane(minimum=0, maximum=1, name="chi_1_in_plane"),
chi_2_in_plane=bilby.gw.prior.ConditionalChiInPlane(minimum=0, maximum=1, name="chi_2_in_plane"),
phi_12 = Uniform(name='phi_12', minimum=0, maximum=2 * np.pi, boundary='periodic'),
phi_jl = Uniform(name='phi_jl', minimum=0, maximum=2 * np.pi, boundary='periodic'),
luminosity_distance = bilby.gw.prior.UniformSourceFrame(name='luminosity_distance', minimum=1e2, maximum=5e3, unit='Mpc'),
dec: Cosine(name='dec'),
ra: Uniform(name='ra', minimum=0, maximum=2 * np.pi, boundary='periodic'),
cos-theta-jn: Uniform(minimum=-1, maximum=1, name='cos_theta_jn'),
psi: Uniform(name='psi', minimum=0, maximum=np.pi, boundary='periodic'),
delta_phase: Uniform(name='delta_phase', minimum=-np.pi, maximum=np.pi, boundary='periodic')
}

################################################################################
## Post processing arguments
################################################################################

postprocessing-executable=None
postprocessing-arguments=None
single-postprocessing-executable=None
single-postprocessing-arguments=None

################################################################################
## Sampler arguments
################################################################################

sampler=bilby_mcmc
sampling-seed=None
n-parallel=13
sampler-kwargs={nsamples=2000, thin_by_nact=0.2, ntemps=8, npool=8, Tmax_from_SNR=20, adapt=True, proposal_cycle='gwA', L1steps=100, L2steps=5}
request-cpus=8

################################################################################
## Waveform arguments
################################################################################

waveform-generator=bilby.gw.waveform_generator.WaveformGenerator
reference-frequency=20
waveform-approximant=IMRPhenomPv2
catch-waveform-errors=False
pn-spin-order=-1
pn-tidal-order=-1
pn-phase-order=-1
pn-amplitude-order=0
numerical-relativity-file=None
waveform-arguments-dict=None
mode-array=None
frequency-domain-source-model=lal_binary_black_hole
conversion-function=None
generation-function=None

