import sys
import os

import bilby
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

from bilby_test import calculate_js

np.random.seed(42)

mcmc_dir = sys.argv[1]
dynesty_dir = sys.argv[2]
compare_dir = f"compare_{mcmc_dir}_{dynesty_dir}".replace("/", "")
if os.path.exists(compare_dir) is False:
    os.mkdir(compare_dir)


mcmc_result_file = f"{mcmc_dir}/result/bbh_A_data0_0_analysis_H1L1_bilby_mcmc_merge_result.json"
mcmc_result = bilby.core.result.read_in_result(mcmc_result_file)
dynesty_result_file = f"{dynesty_dir}/result/bbh_A_data0_0_analysis_H1L1_dynesty_merge_result.json"
dynesty_result = bilby.core.result.read_in_result(dynesty_result_file)

js_medians = {}
keys = []
check_keys = ["chirp_mass", "mass_ratio", "mass_1", "mass_2", "mass_1_source", "mass_2_source", "a_1", "a_2", "chi_eff", "chi_p", "luminosity_distance", "dec", "ra", "theta_jn", "psi", "phase"]
print("JS values:")
N = 5000
samplesA = mcmc_result.posterior.sample(N)
samplesB = dynesty_result.posterior.sample(N)

for key in check_keys:
    js = calculate_js(samplesA[key], samplesB[key], 1)
    js_medians[key] = js.median
    keys.append(key)
    print(f"  {key}: {js.median}")


idx = np.argmax(list(js_medians.values()))
print(f"Maximum JS for {N} samples of {keys[idx]} is {js_medians[keys[idx]]}")

plt.style.use("../paper.mplstyle")
keys = [
    ("mass_1", r"$m_1^{\rm det}$"),
    ("mass_1_source", r"$m_1$"),
    ("mass_2", r"$m_2^{\rm det}$"),
    ("mass_2_source", r"$m_2$"),
    ("chi_eff", r"$\chi_{\rm eff}$"),
    ("chi_p", r"$\chi_{\rm p}$"),
    ("luminosity_distance", "$d_{L}$"),
    ("theta_jn", r"$\theta_{JN}$"),
    ("ra", r"RA"),
    ("dec", r"DEC"),
]


for key, latex in keys:
    fig, ax = plt.subplots()
    dfs = [dynesty_result.posterior, mcmc_result.posterior]
    xmax = np.max([np.max(df[key]) for df in dfs])
    xmin = np.min([np.min(df[key]) for df in dfs])
    cols = ["C0", "C1"]
    labs = ["dynesty", "mcmc"]
    for df, col, lab in zip(dfs, cols, labs):
        sns.histplot(
            df[key], color=col, label=lab, ax=ax, element='step',
            binrange=(xmin, xmax), bins=50, stat='density',
            alpha=0.3, edgecolor=col, linewidth=0.5,
        )

        five = np.quantile(df[key], 0.05)
        ninefive = np.quantile(df[key], 0.95)
        ax.axvline(five, ls='-', color=col, lw=0.25)
        ax.axvline(ninefive, ls='-', color=col, lw=0.25)
        ax.axvspan(five, ninefive, color=col, alpha=0.1)

    ax.set_title(f"JS={1e3 * js_medians[key]:0.1f} [mb]")
    ax.set_xlabel(latex)
    ax.set_yticklabels([])
    ax.axvline(dynesty_result.injection_parameters[key], color='k', ls=':')
    fig.tight_layout()
    fig.savefig(f"{compare_dir}/BBH_1D_{key}")

