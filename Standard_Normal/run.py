#!/usr/bin/env python

import bilby
import numpy as np

from bilby_test import command_line_setup, get_likelihood_prior_and_sample, print_msg

print_msg()
np.random.seed(123)

sampler, sampler_kwargs, outdir, label = command_line_setup()

likelihood, priors, _ = get_likelihood_prior_and_sample("standard_normal")

result = bilby.run_sampler(
    likelihood=likelihood,
    priors=priors,
    sampler=sampler,
    outdir=outdir,
    label=label,
    check_point_plot=True,
    resume=True,
    printdt=5,
    **sampler_kwargs
)
