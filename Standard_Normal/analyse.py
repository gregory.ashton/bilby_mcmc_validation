import sys

import numpy as np
from bilby_test.utils import print_message
from bilby_test import calculate_js

# Pass directories with results on the command line
outdir = sys.argv[1]
label = sys.argv[2]
result_file = f"{outdir}/{label}_result.json"
resume_file = f"{outdir}/{label}_resume.pickle"

print("Analysing Standard Normal")
result, resume = print_message(result_file, resume_file)

priors = result.priors
lnZ = - np.log(priors["x0"].width)
print(f"  Analytic evidence, lnZ={lnZ}")
print(f"  delta lnZ = {result.log_evidence - lnZ} \\pm {result.log_evidence_err}")

samplesA = result.posterior["x0"]
samplesB = np.random.normal(0, 1, 5000)
js_median = calculate_js(samplesA, samplesB).median
print(f"  max-JS / 1e-3 = {js_median / 1e-3}")
