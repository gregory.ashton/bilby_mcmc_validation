import sys

import numpy as np

from bilby_test.standard_tests import cov
from bilby_test import get_likelihood_prior_and_sample, calculate_js
from bilby_test.utils import print_message

_, priors, _ = get_likelihood_prior_and_sample("bimodal_15D_gaussian")

# The prior is constant and flat, and the likelihood is normalised such
# that the area under it is one. The analytical evidence is then given
# as 1/(prior volume).

log_prior_vol = np.sum(np.log(
    [prior.maximum - prior.minimum for key, prior in priors.items()]
))
log_evidence = -log_prior_vol


# Pass directories with results on the command line
outdir = sys.argv[1]
label = sys.argv[2]
result_file = f"{outdir}/{label}_result.json"
resume_file = f"{outdir}/{label}_resume.pickle"

result, resume = print_message(result_file, resume_file)

dRs = []
pos5000 = result.posterior.sample(5000)
for i in range(15):
    xs = pos5000[f"x{i}"]
    dR = np.abs(1 - len(xs[xs > 0]) / len(xs[xs < 0]))
    dRs.append(dR)

print(f"  Analytic lnZ={log_evidence:0.2f}")
print(f"  Result lnZ={result.log_evidence:0.2f}+/-{result.log_evidence_err:0.2f}")
print(f"  delta lnZ = {result.log_evidence - log_evidence} \\pm {result.log_evidence_err}")
print(f"  Max dR = {max(dRs)}")

N = 5000
mean_1 = 4 * np.sqrt(np.diag(cov))
mean_2 = -4 * np.sqrt(np.diag(cov))
dataset_1 = np.random.multivariate_normal(mean_1, cov, size=int(N / 2))
dataset_2 = np.random.multivariate_normal(mean_2, cov, size=int(N / 2))
dataset = np.concatenate([dataset_1, dataset_2], axis=0)

js_medians = []
for ii, key in enumerate(result.search_parameter_keys):
    samplesA = result.posterior[key]
    samplesB = dataset[:, ii]
    js_medians.append(calculate_js(samplesA, samplesB).median)
print(f"  Maximum JS: {1000 * max(js_medians):1.3g} [mb]")


