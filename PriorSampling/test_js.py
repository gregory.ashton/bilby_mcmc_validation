from bilby_test import calculate_js
import bilby

result_file = "outdir_mcmc_chi_A/result/bbh_A_data0_0_analysis_H1L1_bilby_mcmc_merge_result.json"
prior_file = "bbh_sampled.prior"
N = 5000

# Draw prior samples
prior = bilby.gw.prior.BBHPriorDict(prior_file)
samples = prior.sample(N)

# Read in results
result = bilby.core.result.read_in_result(result_file)
posterior = result.posterior

keys = set(prior.non_fixed_keys)

for key in keys:
    samplesA = posterior[key].sample(N)
    samplesB = samples[key]
    js = calculate_js(samplesA, samplesB, 100)
    print(f"JS {key}: {js.median:0.2e} (+{js.plus} -{js.minus})", flush=True)
